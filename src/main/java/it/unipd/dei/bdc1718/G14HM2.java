package it.unipd.dei.bdc1718;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import scala.Tuple2;

import java.io.*;
import java.util.*;
import java.util.concurrent.atomic.AtomicLong;


public class G14HM2 {
    private static class CompareTuple implements Serializable, Comparator<Tuple2<String, Long>> {
        public int compare(Tuple2<String, Long> a, Tuple2<String, Long> b) {
            return (a._2 == b._2) ? 0 : (a._2 < b._2) ? 1 : -1;
        }
    }

    private static class Indexer implements Serializable {
        private AtomicLong l;
        private final long max;

        public Indexer(long max_val) {
            max = max_val;
            l = new AtomicLong(0L);
        }

        public long nextLong() {
            return l.getAndIncrement() % max;
        }
    }

    public static void main(String args[]) {
        if (args[0] == null ||
                !(args[0] instanceof String &&
                        args[0].substring(args[0].length() - 3).equals("txt"))
                ) {
            System.out.println("Expecting a path to a valid .txt file as first argument of the program. Quitting ...");
            System.exit(2);
        }

        int numWords;
        Scanner in = new Scanner(System.in);
        System.out.println("Select how many top words to visualize: ");
        try {
            numWords = in.nextInt();
        } catch (Exception e) {
            System.out.println("An Error occurred, defaulting to 5.");
            numWords = 5;
        }

        // Setup Spark
        SparkConf conf = new SparkConf(true)
                .setAppName("G14HM2_old-test")
                .setMaster("local[*]");
        JavaSparkContext sc = new JavaSparkContext(conf);
        sc.setLogLevel("ERROR");

        int cores = Runtime.getRuntime().availableProcessors();
        JavaRDD<String> docs = sc.textFile(args[0], cores).cache(); //read the file and store it in an RDD with as
        // much partitions as there are cores in the machine
        long N = docs.count();
        long N_sqrt = (long) Math.floor(Math.sqrt(N));

        // ------------------------------------------------NOTICE 1-----------------------------------------------------
        //We measure the time of the task considering also the sorting that takes place to output the <numWords> top
        //recurrent words. This is firstly because we need an 'action' to trigger the effective evaluation of word count,
        //secondly because the particular action <takeOrdered> returns the final output of this homework, therefore it
        //sounds better to us to time this 'action' rather than an useless one (like a <count> or <first> or whatever
        //else).
        //Were us not do this, the time measured would only be what Spark takes to dispatch the informations about
        //the workload to the workers, without the actual computation taking place, due to Spark's lazy evaluation.
        // In such a case, then, the time measurements will almost be constant regardless the actual input file's size.
        //--------------------------------------------------------------------------------------------------------------


        // ------------------------------------------------NOTICE 2-----------------------------------------------------
        //Deterministically assign keys in WC2() and WC3() through the helper class 'Indexer' lets us gain around 10%
        //performance with respect to a random key assignment.
        //--------------------------------------------------------------------------------------------------------------


        //---------------------------------------------"Ground Truth"---------------------------------------------------
        //Use the most basic algorithm to find the word counts, in order to check results of most advanced ones later.
        long startgt = System.currentTimeMillis();
        List<Tuple2<String, Long>> resgt = GroundTruth(docs, numWords);
        long endgt = System.currentTimeMillis();

        //-----------------------------------------"Improved word count 1"----------------------------------------------
        //Implement improved word count 1
        long start1 = System.currentTimeMillis();
        List<Tuple2<String, Long>> res1 = WC1(docs, numWords);
        long end1 = System.currentTimeMillis();

        //-----------------------------------------"Improved word count 2"----------------------------------------------
        //Implement improved word count 2
        long start2 = System.currentTimeMillis();
        List<Tuple2<String, Long>> res2 = WC2(docs, N_sqrt, numWords);
        long end2 = System.currentTimeMillis();

        //-----------------------------------------"use of reduceByKey()"-----------------------------------------------
        //Implement improved word count 2 with reduceByKey()
        long start3 = System.currentTimeMillis();
        List<Tuple2<String, Long>> res3 = WC3(docs, N_sqrt, numWords);
        long end3 = System.currentTimeMillis();

        //--------------------------------------------------OUTPUT------------------------------------------------------
        try {
            System.out.println(formatOutput("ground truth", endgt - startgt, resgt));
            System.out.println(formatOutput(1, end1 - start1, res1));
            System.out.println(formatOutput(2, end2 - start2, res2));
            System.out.println(formatOutput("2 with reduceByKey()", end3 - start3, res3));

            System.out.println("Press 'KEY + <enter>' to finish");
            System.in.read();
        } catch (IOException e) {
            System.out.println("Something went wrong. Quitting...");
            System.out.println(e.getMessage());
            System.exit(3);
        }

        System.exit(0);
    }

    //--------------------------------------------------HELPER FUNCTIONS------------------------------------------------

    /*
        idx: name of the function
        time: runtime of the function
        tuples: list of tuples to display
     */
    public static <K, V> String formatOutput(Object idx, long time, List<Tuple2<K, V>> tuples) {
        String s = String.format("Word Count %s:\n\truntime : \t%d ms\n", idx.toString(), time);
        s += String.format("\tTop %d occurrences found:\n", tuples.size());
        for (Tuple2<K, V> t : tuples) {
            s += String.format("%16s : %8s\n", t._1.toString(), t._2.toString());
        }

        return s;
    }

    /*
        Implement the simplest counting algorithm for reference.
        docs: documents from which to read words
        num: how many top recurrent words to output
     */
    private static List<Tuple2<String, Long>> GroundTruth(JavaRDD<String> docs, int num) {
        return docs
                .flatMapToPair((d) -> {                                                 // Round 1: map phase
                    String[] words = d.split(" ");
                    ArrayList<Tuple2<String, Long>> p = new ArrayList<>();
                    for (String s : words)
                        p.add(new Tuple2<>(s, 1L));

                    return p.iterator();
                })
                .groupByKey()                                                           // Round 1: reduce phase
                .mapValues((list_it) -> {
                    long sum = 0;
                    for (long l : list_it)
                        sum += l;

                    return sum;
                })
                .takeOrdered(num, new CompareTuple());                                  // Round 2: Trigger RDD's evaluation
    }

    /*
          Implement 'improved word count 1'.
          docs: documents from which to read words
          num: how many top recurrent words to output
     */
    private static List<Tuple2<String, Long>> WC1(JavaRDD<String> docs, int num) {
        return docs
                .flatMapToPair((doc) -> {                                               // Round 1: map phase
                    String[] words = doc.split(" ");
                    HashMap<String, Long> tab = new HashMap<>();
                    for (String s : words)
                        if (tab.containsKey(s))
                            tab.replace(s, tab.get(s) + 1L);
                        else
                            tab.put(s, 1L);

                    ArrayList<Tuple2<String, Long>> partial_counts = new ArrayList<>();
                    for (String key : tab.keySet())
                        partial_counts.add(new Tuple2<>(key, tab.get(key)));

                    return partial_counts.iterator();
                })
                .groupByKey()                                                           // Round 1: reduce phase
                .mapValues((list_it) -> {
                    long sum = 0;
                    for (long l : list_it)
                        sum += l;

                    return sum;
                })
                .takeOrdered(num, new CompareTuple());                                  // Round 2: Trigger RDD's evaluation
    }

    /*
          Implement 'improved word count 2'.
          docs: documents from which to read words
          N: number of partition for the data
          num: how many top recurrent words to output
     */
    private static List<Tuple2<String, Long>> WC2(JavaRDD<String> docs, final long N, int num) {
//        Random rng = new Random();
        Indexer idx = new Indexer(N); //deterministically partition data into N groups
        return docs
                .flatMapToPair((doc) -> {                                               // Round 1: map phase
                    String[] words = doc.split(" ");
                    HashMap<String, Long> tab = new HashMap<>();
                    for (String s : words)
                        if (tab.containsKey(s))
                            tab.replace(s, tab.get(s) + 1L);
                        else
                            tab.put(s, 1L);

                    ArrayList<Tuple2<Long, Tuple2<String, Long>>> partial_counts = new ArrayList<>();
                    for (String key : tab.keySet())
                        partial_counts.add(new Tuple2<>(idx.nextLong() /*% N*/, new Tuple2<>(key, tab.get(key))));

                    return partial_counts.iterator();
                })
                .groupByKey()                                                           // Round 1: reduce phase
                .flatMapToPair((it) -> {
                    HashMap<String, Long> words = new HashMap<>();
                    for (Tuple2<String, Long> t : it._2)
                        if (words.containsKey(t._1))
                            words.replace(t._1, words.get(t._1) + t._2);
                        else
                            words.put(t._1, t._2);

                    ArrayList<Tuple2<String, Long>> partial_counts = new ArrayList<>();
                    for (String key : words.keySet())
                        partial_counts.add(new Tuple2<>(key, words.get(key)));

                    return partial_counts.iterator();
                })
                .groupByKey()                                                           // Round 2: reduce phase
                .mapValues((list_it) -> {
                    long sum = 0;
                    for (long l : list_it)
                        sum += l;

                    return sum;
                })
                .takeOrdered(num, new CompareTuple());                                  // Round 3: Trigger RDD's evaluation
    }

    /*
        Implement 'improved word count 2', but use reduceByKey() when possible.
        docs: documents from which to read words
        N: number of partition for the data
        num: how many top recurrent words to output
    */
    public static List<Tuple2<String, Long>> WC3(JavaRDD<String> docs, final long N, int num) {
//        Random idx = new Random();
        Indexer idx = new Indexer(N); //deterministically partition data into N groups
        return docs
                .flatMapToPair((doc) -> {                                               // Round 1: map phase
                    String[] words = doc.split(" ");
                    HashMap<String, Long> tab = new HashMap<>();
                    for (String s : words)
                        if (tab.containsKey(s))
                            tab.replace(s, tab.get(s) + 1L);
                        else
                            tab.put(s, 1L);

                    ArrayList<Tuple2<Long, Tuple2<String, Long>>> partial_counts = new ArrayList<>();
                    for (String key : tab.keySet())
                        partial_counts.add(new Tuple2<>(idx.nextLong() /*% N*/, new Tuple2<>(key, tab.get(key))));

                    return partial_counts.iterator();
                })
                .groupByKey()                                                           // Round 1: reduce phase
                .flatMapToPair((it) -> {
                    HashMap<String, Long> words = new HashMap<>();
                    for (Tuple2<String, Long> t : it._2)
                        if (words.containsKey(t._1))
                            words.replace(t._1, words.get(t._1) + t._2);
                        else
                            words.put(t._1, t._2);

                    ArrayList<Tuple2<String, Long>> partial_counts = new ArrayList<>();
                    for (String key : words.keySet())
                        partial_counts.add(new Tuple2<>(key, words.get(key)));

                    return partial_counts.iterator();
                })
                .reduceByKey((v1, v2) -> v1 + v2)                                       // Round 2: reduce phase
                .takeOrdered(num, new CompareTuple());                                  // Round 3: Trigger RDD's evaluation
    }

}