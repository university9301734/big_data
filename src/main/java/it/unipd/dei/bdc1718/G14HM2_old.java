package it.unipd.dei.bdc1718;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import scala.Tuple2;

import java.io.IOException;
import java.io.Serializable;
import java.sql.Time;
import java.util.*;

public class G14HM2_old {

    private static class CompareTuple implements Serializable, Comparator<Tuple2<String, Long>>{
            public int compare(Tuple2<String, Long> a, Tuple2<String, Long> b)
            {
                if (a._2 ==  b._2)   return 0;
                return (a._2 <  b._2)? 1 : -1;
            }
    }


    public static JavaPairRDD<String, Long> stupidWC(JavaRDD<String> docs){
        JavaPairRDD<String, Long> wordcounts = docs
                .flatMapToPair((document) -> {             // <-- Map phase
                    String[] tokens = document.split(" ");
                    ArrayList<Tuple2<String, Long>> pairs = new ArrayList<>();
                    for (String token : tokens) {
                        pairs.add(new Tuple2<>(token, 1L));
                    }
                    return pairs.iterator();
                })
                .groupByKey()                       // <-- Reduce phase
                .mapValues((it) -> {
                    long sum = 0;
                    for (long c : it) {
                        sum += c;
                    }
                    return sum;
                });
        return  wordcounts;
    }


        public static JavaPairRDD<String, Long> improvedWC1(JavaRDD<String> docs){
        JavaPairRDD<String, Long> wordcounts = docs
                .flatMapToPair((document) -> {             // <-- Map phase
                    String[] tokens = document.split(" ");
                    HashMap<String,Long> counter = new HashMap<>();
                    ArrayList<String> words = new ArrayList<>();
                    for (String token : tokens) {
                        long wordCount = 1;
                        if(counter.containsKey(token)){
                            wordCount += counter.get(token);
                            counter.remove(token);
                        }else{
                            words.add(token);
                        }
                        counter.put(token, wordCount);
                    }

                    ArrayList<Tuple2<String, Long>> pairs = new ArrayList<>();
                    for (String word : words) {
                        pairs.add(new Tuple2<>(word, counter.get(word)));
                    }
                    return pairs.iterator();
                })
                .groupByKey()                       // <-- Reduce phase
                .mapValues((it) -> {
                    long sum = 0;
                    for (long c : it) {
                        sum += c;
                    }
                    return sum;
                });
        return wordcounts;
    }



    public static JavaPairRDD<String, Long> improvedWC2(JavaRDD<String> docs, long N){
        Random random = new Random();
        int sqrtn = (int)Math.sqrt(N);
        JavaPairRDD<String, Long> wordcounts = docs
                //.repartition((int) Math.sqrt(N))
                .flatMapToPair((document) -> {             // <-- Map phase
                    String[] tokens = document.split(" ");
                    HashMap<String,Long> counter = new HashMap<>();
                    ArrayList<String> words = new ArrayList<>();
                    for (String token : tokens) {
                        //long wordCount = 1;
                        if(counter.containsKey(token))
                        //{
                            counter.replace(token, counter.get(token) + 1L);
                            //wordCount += counter.get(token);
                            //counter.remove(token);
                        //}
                        //counter.put(token, wordCount);
                        else {
                            counter.put(token, 1L);
                            words.add(token);
                        }
                    }

                    ArrayList<Tuple2<Long,Tuple2<String, Long>>> pairs = new ArrayList<>();
                    for (String word : words) {
                        pairs.add(new Tuple2<>(random.nextLong()%sqrtn,
                                new Tuple2<>(word, counter.get(word))));
                    }
                    return pairs.iterator();
                })
                .groupByKey()                       // <-- Reduce phase
                .flatMapToPair((it) -> {
                    ArrayList<String> keys = new ArrayList<>();
                    HashMap<String,Long> counter = new HashMap<>();
                    for (Tuple2<String,Long> t: it._2) {
                        long wordCount = t._2;
                        if(counter.containsKey(t._1)){
                            wordCount += counter.get(t._1);
                            counter.remove(t._1);
                        }else{
                            keys.add(t._1);
                        }
                        counter.put(t._1, wordCount);
                    }

                    ArrayList<Tuple2<String, Long>> pairs = new ArrayList<>();
                    for (String s : keys) {
                        pairs.add(new Tuple2<>(s, counter.get(s)));
                    }

                    //return counter.values();
                    return pairs.iterator();
                })
                .groupByKey()
                .mapValues((it) -> {
                    long sum = 0;
                    for (long c : it) {
                        sum += c;
                    }
                    return sum;}
                    );
        return wordcounts;
    }

    public static JavaPairRDD<String, Long> improvedWC3(JavaRDD<String> docs){

        JavaPairRDD<String, Long> wordcounts = docs
                .flatMapToPair((document) -> {             // <-- Map phase
                    String[] tokens = document.split(" ");
                    ArrayList<Tuple2<String,Long>> list= new ArrayList<>();
                    for (String token : tokens) {
                        list.add(new Tuple2<>(token,1L));
                    }
                    return list.iterator();
                })
                .reduceByKey((x,y) -> (x+y));
        return wordcounts;
    }


    public static void main(String[] args){
        // Setup Spark
        SparkConf conf = new SparkConf(true)
                .setAppName("HW2")
                .setMaster("local[*]");
        JavaSparkContext sc = new JavaSparkContext(conf);

        String filename = args[0];
        int k = Integer.parseInt(args[1]);

        JavaRDD<String> docs = sc.textFile(filename).cache();
        long N = docs.count();

        // Now the RDD has been loaded and cached in memory and
        // we can start measuring time and calling functions

        long start = System.currentTimeMillis();
        JavaPairRDD<String, Long> wc = stupidWC(docs);
        long timeWC = System.currentTimeMillis() - start;

        start = System.currentTimeMillis();
        JavaPairRDD<String, Long> wc1 = improvedWC1(docs);
        long timeWC1 = System.currentTimeMillis() - start;

        start = System.currentTimeMillis();
        JavaPairRDD<String, Long> wc2 = improvedWC2(docs, N);
        long timeWC2 = System.currentTimeMillis() - start;

        start = System.currentTimeMillis();
        JavaPairRDD<String, Long> wc3 = improvedWC3(docs);
        long timeWC3 = System.currentTimeMillis() - start;


        List<Tuple2<String,Long>> topK = wc.takeOrdered(k, new CompareTuple());
        List<Tuple2<String,Long>> topK1 = wc1.takeOrdered(k, new CompareTuple());
        List<Tuple2<String,Long>> topK2 = wc2.takeOrdered(k, new CompareTuple());
        List<Tuple2<String,Long>> topK3 = wc3.takeOrdered(k, new CompareTuple());
        for (int i=0; i<k; i++) {
            System.out.println(topK.get(i)._1+" : "+topK.get(i)._2);
            System.out.println(topK1.get(i)._1+" : "+topK1.get(i)._2);
            System.out.println(topK2.get(i)._1+" : "+topK2.get(i)._2);
            System.out.println(topK3.get(i)._1+" : "+topK3.get(i)._2);
        }

        System.out.println("Time elapsed WC1: "+timeWC1);
        System.out.println("Time elapsed WC2: "+timeWC2);
        System.out.println("Time elapsed WC3: "+timeWC3);

        System.out.println("Press enter to finish main program");
        try {
            System.in.read();
        } catch (IOException e) {
        }
    }
}
