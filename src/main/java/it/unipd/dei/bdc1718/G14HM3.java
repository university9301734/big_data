package it.unipd.dei.bdc1718;

import org.apache.spark.mllib.linalg.Vector;
import org.apache.spark.mllib.linalg.Vectors;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

public class G14HM3 {

    // must run in O(|P|*k)
    public static ArrayList<Vector> kcenter(ArrayList<Vector> P, int k){
        if(k >= P.size())   return P;

        ArrayList<Vector> sol = new ArrayList<>();
        Random rg = new Random();
        sol.add(P.get(rg.nextInt(P.size()))); //first center, chosen at random

        double[] distances = new double[P.size()];// contains the maximum of the minimum squared 
                                                  //distances from sol to the i-th point
        Vector c1 = sol.get(0);
        for (int i = 0; i<P.size(); i++)    distances[i] = Vectors.sqdist(c1, P.get(i));

        for (int i = 1; i < k; i++){
            Vector sel = null;
            double dist = 0;
            for (int j = 0; j < P.size(); j++){
                double tmp_dist = Vectors.sqdist(P.get(j), sol.get(i-1)); //only the last extracted center is relevant
                if( tmp_dist < distances[j]){
                    distances[j] = tmp_dist; //if P[j] == sol[i-1] --> tmp_dist = 0, but this is an intended
                                            // behaviour, to avoid points already in sol
                }

                //search, progressively, for the maximum distance
                if(dist < distances[j]){
                    dist = distances[j];
                    sel = P.get(j); 
                }
            }
            sol.add(sel); //append the maximum distance center
        }
        return sol;
    }

    // must run in O(|P|*k)
    public static ArrayList<Vector> kmeansPP(ArrayList<Vector> P, ArrayList<Long> WP, int k){
        if(k >= P.size())   return P;

        ArrayList<Vector> sol = new ArrayList<>();
        Random rg = new Random();
        Vector c1 = P.get(rg.nextInt(P.size())); //get the first random center
        sol.add(c1);
        double[] distances2 = new double[P.size()]; // contains the maximum of the minimum squared
                                                    //distances from sol to the i-th point
        double sumDistances2 = 0;
        for (int i = 0; i < P.size(); i++){
            double d2 = Vectors.sqdist(c1, P.get(i));
            distances2[i] = d2 /* * d2 */ * WP.get(i); //Vector.sqdist() already returns the squared distance
            sumDistances2 += distances2[i];
        }

        for (int i = 1; i < k; i++){
            for (int j = 0; j < P.size(); j++){
                double d2 = Vectors.sqdist(sol.get(i-1), P.get(j)); //only the last extracted center is relevant
                d2 = d2 /* * d2 */ * WP.get(j); //Vector.sqdist() already returns the squared distance
                if( d2 < distances2[j]){
                    sumDistances2 -= distances2[j]; //keep normalization factor up to date
                    distances2[j] = d2;
                    sumDistances2 += distances2[j];
                }
            }

            double x = rg.nextDouble()  * sumDistances2; //working with non normalized probabilities
                                                         //lets us spare some (potentially many)
                                                         //division operations in the for cicle
            double cum_sum = 0;
            int z = 0;
            for( ; z < P.size() && cum_sum <= x; z++) cum_sum += distances2[z]  /* / sumDistances2 */;

            sol.add(P.get(--z)); //found an interesting center

        }
        return sol;
    }

    public static double kmeansObj(ArrayList<Vector> P, ArrayList<Vector> C){
        double sol = 0;
        for (final Vector p : P){
            double dmin = Double.MAX_VALUE;
            for (final Vector c : C){
                dmin = Math.min(Vectors.sqdist(c, p), dmin);
            }
            sol += dmin;
        }
        return sol/P.size();
    }

    public static void main(String args[]) {
        //--------------------------------------------------CHECK INPUTS---------------------------------------------
        int k = 250, k1 = 1000; //default values
        if (args.length == 0)
        {
            System.out.println("Expecting the INPUT TEXT FILE as first argument of the program. ABORTING.");
            System.exit(1);
        }

        if(args.length == 3)
        {
            k = Integer.parseInt(args[1]);
            k1 = Integer.parseInt(args[2]);
        }
        else
        {
            System.out.println("Using default values for k and k1");
        }

        if(k > k1)
        {
            System.out.println("Found k > k1. Swapping their values so that k < k1.");
            int tmp = k;
            k = k1;
            k1 = tmp;
        }

        //--------------------------------------------------LOAD THE FILE---------------------------------------------
        ArrayList<Vector> points = null;
        try {
            System.out.println("Loading file ...");
            points = InputOutput.readVectorsSeq(args[0]);
            System.out.println("File loaded, starting ...");
        }catch(IOException e)
        {
            System.out.println("COULD NOT READ INPUT FILE: "+ e.getMessage() + "\n\nABORTING");
            System.exit(2);
        }

        //-----------------------------------------------------ES 1--------------------------------------------------
        long t1 = System.currentTimeMillis();
        ArrayList<Vector> ppp =  kcenter(points, k);
        long time = System.currentTimeMillis() - t1;
        System.out.println("1) Kcenter took "+time+" ms");

        //-----------------------------------------------------ES 2--------------------------------------------------
        ArrayList<Long> weights1 = new ArrayList<>();
        for(int i=0; i<points.size(); i++) weights1.add(1L);
        double firstSol = kmeansObj(points, kmeansPP(points, weights1, k));
        System.out.println(String.format("2) kmeans++ gives kmeansObj = %.3f", firstSol));


        //-----------------------------------------------------ES 3--------------------------------------------------
        Random r = new Random();
        ArrayList<Vector> X = kcenter(points, k1);
        ArrayList<Long> weights2 = new ArrayList<>();
        for(int i = 0; i < X.size(); i++) weights2.add(Math.abs(r.nextLong() % k));
        double secondSol = kmeansObj(points, kmeansPP(X, weights2, k));
        System.out.println(String.format("3) Refined coreset gives kmeansObj = %.3f", secondSol));

        System.exit(0);
    }
}
