package it.unipd.dei.bdc1718;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.mllib.linalg.Vector;
import org.apache.spark.mllib.linalg.Vectors;
import scala.Tuple2;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class G14HM4 {

    /**
     * Sequential approximation algorithm based on matching.
     */
    public static ArrayList<Vector> runSequential(final ArrayList<Vector> points, int k) {
        final int n = points.size();
        if (k >= n) {
            return points;
        }

        ArrayList<Vector> result = new ArrayList<>(k);
        boolean[] candidates = new boolean[n];
        Arrays.fill(candidates, true);
        for (int iter=0; iter<k/2; iter++) {
            // Find the maximum distance pair among the candidates
            double maxDist = 0;
            int maxI = 0;
            int maxJ = 0;
            for (int i = 0; i < n; i++) {
                if (candidates[i]) {
                    for (int j = i+1; j < n; j++) {
                        if (candidates[j]) {
                            double d = Math.sqrt(Vectors.sqdist(points.get(i), points.get(j)));
                            if (d > maxDist) {
                                maxDist = d;
                                maxI = i;
                                maxJ = j;
                            }
                        }
                    }
                }
            }
            // Add the points maximizing the distance to the solution
            result.add(points.get(maxI));
            result.add(points.get(maxJ));
            // Remove them from the set of candidates
            candidates[maxI] = false;
            candidates[maxJ] = false;
        }
        // Add an arbitrary point to the solution, if k is odd.
        if (k % 2 != 0) {
            for (int i = 0; i < n; i++) {
                if (candidates[i]) {
                    result.add(points.get(i));
                    break;
                }
            }
        }
        if (result.size() != k) {
            throw new IllegalStateException("Result of the wrong size");
        }
        return result;
    }

    private static Vector fromString(String s)
    {
        String[] tokens = s.split(" ");
        double[] vals = new double[tokens.length];
        for (int i = 0; i < vals.length; i++)
            vals[i] = Double.parseDouble(tokens[i]);
        return Vectors.dense(vals);
    }

    /**
     * Function required by the homework
     */
    public static ArrayList<Vector> runMapReduce(JavaRDD<Vector> pointsrdd, int k, int numBlocks){
        Random rand = new Random();

        return  pointsrdd.mapToPair( (vector) -> (new Tuple2<Integer, Vector> (rand.nextInt(numBlocks), vector)))
                .groupByKey()   // es 1A
                .mapToPair(
                        (it) -> {
                            ArrayList<Vector> v = new ArrayList<>();
                            for (Vector vec : it._2) {
                                v.add(vec);
                            }
                            ArrayList<Vector> kcen = G14HM3.kcenter(v, k);
                            return new Tuple2<>(0, kcen);
                        }) // es 1B
                .groupByKey()
                .map( (it) -> {
                    ArrayList<Vector> res = new ArrayList<>();
                    for (ArrayList<Vector> a : it._2){
                        res.addAll(a);
                    }
                    return res;
                })
                .collect().get(0);
    }

    public static double measure(ArrayList<Vector> pointslist)
    {
        int k = pointslist.size();

        double den = k * (k - 1) / 2;

        double num = 0;
        for (int i = 0; i < k; i++) {
            Vector v1 = pointslist.get(i);
            for (int j = i+1; j < k; j++) {
                Vector v2 = pointslist.get(j);
                num += Math.sqrt(Vectors.sqdist(v1, v2));
            }
        }

        return num / den;
    }

    public static void main(String[] args)
    {
        //System.setProperty("hadoop.home.dir", "C:\\Bin\\Winutils\\"); // to fix hadoop problems in Dario's PC
        SparkConf conf = new SparkConf(true)
                .setAppName("HW4-G14")
                .setMaster("local[*]");
        JavaSparkContext sc = new JavaSparkContext(conf);

        int numBlocks;
        try {
            numBlocks = Integer.parseInt(args[2]);
        }
        catch (Exception e) {
            System.out.println("Defaulting numBlocks to 8. \n" + e.getMessage());
            numBlocks = 8;
        }

        int k;
        try {
            k = Integer.parseInt(args[1]);
        }
        catch (Exception e) {
            System.out.println("Defaulting k to 1000. \n" + e.getMessage());
            k = 1000;
        }

        JavaRDD<Vector> inputrdd = sc.textFile(args[0]).map(G14HM4::fromString).repartition(numBlocks).cache();
        inputrdd.first();
        double mapred_start = System.currentTimeMillis();
        ArrayList<Vector> temp_res = runMapReduce(inputrdd, k, numBlocks);
        double mapred_stop = System.currentTimeMillis();

        double res_start = System.currentTimeMillis();
        ArrayList<Vector> final_res = runSequential(temp_res, k);
        double res_stop = System.currentTimeMillis();


        System.out.println(String.format("1) Average distance: %.3f", measure(final_res)));
        System.out.println(String.format("2) Time for coreset construction: %.3f s", (mapred_stop - mapred_start)/1000));
        System.out.println(String.format("3) Time for final solution: %.3f s", (res_stop - res_start)/1000));
        System.exit(0);
    }
}
