package it.unipd.dei.bdc1718;

import org.apache.spark.mllib.linalg.Vector;

import java.util.ArrayList;
import java.util.Random;

public class G14HM3Dario {

    // must run in O(|P|*k)
    public static ArrayList<Vector> kcenter(ArrayList<Vector> P, int k){
        if(k == P.size())   return P;
        ArrayList<Vector> sol = new ArrayList<>();
        Random rg = new Random();
        Vector initial = P.get(Math.abs(rg.nextInt(P.size())));
        sol.add(initial);
        // contains the maximum of the minimum distances from sol to the i-th point
        double[] distances = new double[P.size()];
        for (int i = 0; i<P.size(); i++)    distances[i] = org.apache.spark.mllib.linalg.Vectors.sqdist(initial,P.get(i));


        for (int i=0; i<k-1; i++){
            int nextPointIndex = 0;
            double maxDistance = distances[nextPointIndex];
            for (int j = 1; j<P.size(); j++){
                if(maxDistance < distances[j]){
                    maxDistance = distances[j];
                    nextPointIndex = j;
                }
            }
            Vector nextPoint = P.get(nextPointIndex);
            sol.add(nextPoint);
            for (int j = 0; j<P.size(); j++){
                if(org.apache.spark.mllib.linalg.Vectors.sqdist(nextPoint,P.get(j)) < distances[j]){
                    distances[j] = org.apache.spark.mllib.linalg.Vectors.sqdist(nextPoint,P.get(j));
                }
            }
        }
        return sol;
    }

    // must run in O(|P|*k)
    public static ArrayList<Vector> kmeansPP(ArrayList<Vector> P, ArrayList<Long> WP, int k){
        if(k == P.size())   return P;
        ArrayList<Vector> sol = new ArrayList<>();
        Random rg = new Random();
        Vector initial = P.get(Math.abs(rg.nextInt(P.size())));
        sol.add(initial);
        // contains the maximum of the minimum distances from sol to the i-th point
        double[] distances = new double[P.size()];
        double pi[] = new double[P.size()];
        double sumDistances2 = 0;
        for (int i = 0; i<P.size(); i++){
            distances[i] = org.apache.spark.mllib.linalg.Vectors.sqdist(initial,P.get(i));
            sumDistances2 += WP.get(i)*distances[i];
        }
        for(int i=0; i<P.size();i++){
            pi[i]= WP.get(i)*distances[i]/sumDistances2;
        }

        for (int i=0; i<k-1; i++){
            double chosen = rg.nextDouble();
            double currentVal = 0;
            int currentIndex=0;
            while (currentVal <= chosen && currentIndex<P.size()) currentVal += pi[currentIndex++];
            currentIndex--;

            Vector nextPoint = P.get(currentIndex);

            sol.add(nextPoint);
            for (int j = 0; j<P.size(); j++){
                if(org.apache.spark.mllib.linalg.Vectors.sqdist(nextPoint,P.get(j)) < distances[j]){
                    sumDistances2 -= WP.get(j)*distances[j];
                    distances[j] = org.apache.spark.mllib.linalg.Vectors.sqdist(nextPoint,P.get(j));
                    sumDistances2 += WP.get(j)*distances[j];
                }
            }
            for(int j=0; j<P.size();j++){
                pi[j]= WP.get(j)*distances[j]/sumDistances2;
            }

        }
        return sol;
    }

    public static double kmeansObj(ArrayList<Vector> P, ArrayList<Vector> C){
        double sol = 0;
        for (int i = 0; i < P.size(); i++){
            double dmin = org.apache.spark.mllib.linalg.Vectors.sqdist(C.get(0),P.get(i));
            for (int j = 1; j < C.size(); j++){
                if(dmin > org.apache.spark.mllib.linalg.Vectors.sqdist(C.get(j),P.get(i)))
                    dmin = org.apache.spark.mllib.linalg.Vectors.sqdist(C.get(j),P.get(i));
            }
            sol += dmin;
        }
        return sol/P.size();
    }

    public static void main(String args[]) throws Exception{
        if(args.length  < 3)  throw new Exception();
        int k=Integer.parseInt(args[1]), k1 = Integer.parseInt(args[2]);
        if(k>=k1)   throw new Exception();
        ArrayList<Vector> points = InputOutput.readVectorsSeq(args[0]);

        long t1 = System.currentTimeMillis();
        kcenter(points, k);
        long time = System.currentTimeMillis() - t1;
        System.out.println("Kcenter took "+time+" ms");
        //System.out.println(points.size());

        ArrayList<Long> weights = new ArrayList<>();
        for(int i=0; i<points.size();i++) weights.add(1L);
        t1 = System.currentTimeMillis();
        double firstSol = kmeansObj(points, kmeansPP(points,weights,k));
        time = System.currentTimeMillis() - t1;
        System.out.println("[2]: "+time);
        System.out.println("kmeans++ gives value kmeansObj="+firstSol);

        t1 = System.currentTimeMillis();
        ArrayList<Vector> X = kcenter(points,k1);
        double secondSol = kmeansObj(points, kmeansPP(X,weights,k));
        time = System.currentTimeMillis() - t1;
        System.out.println("[3]: "+time);
        System.out.println("second request="+secondSol);

    }
}