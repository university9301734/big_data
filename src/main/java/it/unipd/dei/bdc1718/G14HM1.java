package it.unipd.dei.bdc1718;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Scanner;

public class G14HM1 {

    private static class CompareDoubles implements Serializable, Comparator<Double>
    {
        public int compare(Double a, Double b)
        {
            if (a.equals(b))   return 0;
            return (a < b)? -1 : 1;
        }
    }

    public static void main(String[] args) throws FileNotFoundException {
        if (args.length == 0) {
            throw new IllegalArgumentException("Expecting the file name on the command line");
        }

        // -------------------------------------------ES 1----------------------------------------------
        // Read a list of numbers from the program options
        ArrayList<Double> lNumbers = new ArrayList<>();
        Scanner s = new Scanner(new File(args[0]));
        while (s.hasNext()) {
            lNumbers.add(Double.parseDouble(s.next()));
        }
        s.close();

        // Setup Spark
        SparkConf conf = new SparkConf(true)
                .setAppName("Preliminaries");
        JavaSparkContext sc = new JavaSparkContext(conf);

        // Create a parallel collection
        JavaRDD<Double> dNumbers = sc.parallelize(lNumbers);
        long n = dNumbers.count();


        // -------------------------------------------ES 2----------------------------------------------
        double sum = dNumbers.reduce((x, y) -> x + y);  //map function is the identity
        double mean = sum/n;  //compute the average of dataset elements
        JavaRDD<Double> dDiffavgs =  dNumbers.map((x)->Math.abs(x-mean));


        // -------------------------------------------ES 3a---------------------------------------------
        double minval = dDiffavgs.reduce((x,y)-> (x <= y) ? x : y ); //find the minimum value using only reduce() function


        // -------------------------------------------ES 3b---------------------------------------------
        double minval2 = dDiffavgs.min(new CompareDoubles()); //find the minimum value using the API function


        // -------------------------------------------ES 4----------------------------------------------
        //Find empiric unbiased variance, then filter all data that are beyond   mean +/- sqrt(var), finally find the max

        double std = Math.sqrt(dNumbers.map((x) -> (x-mean)*(x-mean)).reduce((x,y)-> x + y) / (n-1));
        dNumbers = dNumbers.map((x) -> (x-mean)).filter((x) -> Math.abs(x) < std);
        double max = dNumbers.max(new CompareDoubles());


        //-------------------------------------------output---------------------------------------------

        //Do the following to enable us to inspect the GUI in the browser
        s = new Scanner(System.in);

        try
        {
            // show output after all Spark's state output
            Thread.sleep(500);
        }
        catch(InterruptedException e)
        {
            System.out.println("ES 3a: Min of dDiffavgs is " + minval);
            System.out.println("ES 3b: Min of dDiffavgs is " + minval2);
            System.out.println("ES 4 : Max of filtered dNumbers is " + max);
        }

        System.out.println("ES 3a: Min of dDiffavgs is " + minval);
        System.out.println("ES 3b: Min of dDiffavgs is " + minval2);
        System.out.println("ES 4 : Max of filtered dNumbers is " + max);

        System.out.println("WAITING FOR ANY 'KEY + <enter>' PRESS TO EXIT");
        while(!s.hasNext())  ; //block and wait

        System.exit(0);
    }


}
